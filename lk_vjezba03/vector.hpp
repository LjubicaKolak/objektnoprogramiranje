#ifndef VECTOR_HPP
#define VECTOR_HPP
#include <vector>

using std::vector;

//1.zadatak
void input_vector(vector <int>& v, int size_vec);
void input_vector2(vector <int>& v, int a, int b);
void print_vector(vector <int> v);


//3.zadatak
void sort_vector(vector <int>& v);

//4.zadatak
void remove_element(vector <int>& v, int position);
#endif
